<?php
require_once 'Jutro.php';


class VirtualProduct extends Jutro
{
//    private $attachment;

    /**
     * VirtualProduct constructor.
     * @param $attachment
     */
    public function __construct($id, $name, $price, $curency, $description, $pictures, $manufacturer, $category, $attachment)
    {
        parent::__construct($id, $name, $price, $curency, $description, $pictures, $manufacturer, $category);
//        $this->attachment = $attachment;
    }


    public function __toString()
    {
        return $this->getName();
        // TODO: Implement __toString() method.
    }
}