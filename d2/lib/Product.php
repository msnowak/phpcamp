<?php
require_once 'Jutro.php';


class Product extends Jutro
{

//    protected $quantity;
//
//    protected $weight;

    /**
     * Product constructor.
     * @param $id
     * @param $name
     * @param $price
     * @param $curency
     * @param $quantity
     * @param $description
     * @param $pictures
     * @param $manufacturer
     * @param $category
     * @param $weight
     */
    public function __construct($id, $name, $price, $curency, $description, $pictures, $manufacturer, $category, $quantity, $weight)
    {
        parent::__construct($id, $name, $price, $curency, $description, $pictures, $manufacturer, $category);
        //$this->setQuantity($quantity);
        //$this->setWeight($weight);
//        $this->quantity = $quantity;
//        $this->weight = $weight;
    }


    public function __toString()
    {
        return $this->getName();
        // TODO: Implement __toString() method.
    }
}