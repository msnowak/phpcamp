<?php
require_once 'ProductsAPI.php';

echo "<br><br><strong>Cześć Mateusz!</strong>";

$products = new ProductsAPI();

$dane = $products->getAllProducts();

if(isset($_GET['action'])){
    switch ($_GET['action']){
        case 'checkProduct':
            $dane = $products->checkProduct($_GET['name']);
            break;

        case 'addProduct':
            $dane = $products->addProduct($_GET['name'],$_GET['price']);
            break;

        case 'removeProduct':
            $dane = $products->removeProduct($_GET['id']);
            break;

        case 'getAll':
            $dane = $products->getAllProducts();
    }
}
//echo json_encode($dane);
header("Content-type: text/xml");
xml_from_indexed_array($dane, 'products', 'product');



function xml_from_indexed_array($array, $parent, $name) {
    echo '<?xml version="1.0"?>', "\n";
    echo "<$parent>\n";
    foreach ($array as $element) {
        echo "  <$name>\n";
        foreach ($element as $child => $value) {
            echo "    <$child>$value</$child>\n";
        }
        echo "  </$name>\n";
    }
    echo "</$parent>\n";
}
