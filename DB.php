<?php
require_once 'DBInterface.php';
error_reporting(E_ALL ^ E_NOTICE);
//komentarz

class DB implements DBInterface
{

    private $host;
    private $login;
    private $password;
    private $dbName;

    private $queryResult;

    private $connection;
    public function __construct($host, $login, $pass, $dbName)
    {
        $this->host = $host;
        $this->dbName = $dbName;
        $this->login = $login;
        $this->password = $pass;

        try{
            $this->connection = new PDO('mysql:host='.$host.';dbname='.$dbName,$this->login, $this->password);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function query($query)
    {
        // TODO: Implement query() method.
        if($this->queryResult = $this->connection->query($query))
            return true;
        else
            return false;
//        if($this->queryResult->execute()) return true;
//        else return false;
    }

    public function getAffectedRows()
    {
        $this->queryResult->execute();
        return $this->queryResult->rowCount();
        // TODO: Implement getAffectedRows() method.

    }

    public function getRow()
    {
        $this->queryResult->execute();
        return $this->queryResult->fetch(PDO::FETCH_ASSOC);
        // TODO: Implement getRow() method.
    }

    public function getAllRows()
    {
        $this->queryResult->execute();
        return $this->queryResult->fetchAll(PDO::FETCH_ASSOC);
        // TODO: Implement getAllRows() method.
    }
}