<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 30.05.2017
 * Time: 16:18
 */
class ServiceFunctions {
    public function getInitials($firstName, $lastName) {
        $name = strtoupper(substr($firstName, 0, 1));
        $name .= ' ' . strtoupper(substr($lastName, 0, 1));
        return $name;
    }
}

$options = Array('uri' => 'http://localhost');
$server = new SoapServer(NULL, $options);
$server->setClass('ServiceFunctions');
$server->handle();