<?php


interface DBInterface
{
    public function __construct($host,$login,$pass,$dbName);

    public function query($query);

    public function getAffectedRows();

    public function getRow();

    public function getAllRows();
}