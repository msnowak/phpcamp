<?php


class Jutro
{
//    protected $id;
//
//    protected $name;
//
//    protected $price;
//
//    protected $curency;
//
//    protected $description;
//
//    protected $pictures;
//
//    protected $manufacturer;
//
//    protected $category;

    /**
     * BaseProduct constructor.
     * @param $id
     * @param $name
     * @param $price
     * @param $curency
     * @param $description
     * @param $pictures
     * @param $manufacturer
     * @param $category
     */
    public function __construct($id, $name, $price, $curency, $description, $pictures, $manufacturer, $category)
    {
//        $this->id = $id;
//        $this->name = $name;
//        $this->price = $price;
//        $this->curency = $curency;
//        $this->description = $description;
//        $this->pictures = $pictures;
//        $this->manufacturer = $manufacturer;
//        $this->category = $category;

//        $this->setId($id);
//        $this->setName($name);
//        $this->setPrice($price);
//        $this->setCurency($curency);
//        $this->setDescription($description);
//        $this->setPictures($pictures);
//        $this->setManufacturer($manufacturer);
//        $this->setCategory($category);
    }

    public function __set($name,$value){
        $this->$name = $value;
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __call($name, $arguments)
    {
        $arg = $arguments[0];
        $matches = [];
        if(preg_match("/set([A-Za-z]{1,})/",$name, $matches)){
            if(isset($matches[0])){
                $val = substr(strtolower($matches[0]),3);
                $this->$val = $arguments[0];
            }
        }
        else if(preg_match("/get([A-Za-z]{1,})/",$name, $matches)){
            if(isset($matches[0])){
                $val = substr(strtolower($matches[0]),3);
                return $this->$val;
            }
        }
    }


}