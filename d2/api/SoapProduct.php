<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
require_once 'ProductsAPI.php';
require_once '../../DB.php';

class SoapProduct
{

    public function checkProduct($name){
        $products = new ProductsAPI();
        return $products->checkProduct($name);
    }

    public function addProduct($name, $price){
        $products = new ProductsAPI();
        return $products->addProduct($name, $price);
    }

    public function removeProduct($id){
        $products = new ProductsAPI();
        return $products->removeProduct($id);
    }

    public function getAllProducts(){
        $products = new ProductsAPI();
        return json_encode($products->getAllProducts());
    }
}


$options = array(
    'uri'=>'http://localhost/'
);

$server = new SoapServer(NULL,$options);
$server->setClass('SoapProduct');
$server->handle();