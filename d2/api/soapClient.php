<?php
$options = array(
    'uri' => 'http://localhost',
    'location' => 'http://localhost/web/phpcamp/d2/api/SoapProduct.php',
    'trace' => 1
);
$client = new SoapClient(NULL,$options);

$data = [];

if(isset($_GET['action'])){
    switch ($_GET['action']){
        case 'checkProduct':
            if(isset($_GET['id']))
                $data = $client->checkProduct($_GET['id']);
            break;

        case 'addProduct':
            if(isset($_GET['name']) && isset($_GET['price']))
                if((float)$_GET['price'] > 0)
                    $data = $client->addProduct($_GET['name'],$_GET['price']);
            break;

        case 'removeProduct':
            if(isset($_GET['id']) && is_numeric($_GET['id']))
                $data = $client->removeProduct($_GET['id']);
            break;

        case 'getAll':
            $data = $client->getAllProducts();
    }
}
var_dump($data);
//var_dump($client->getAllProducts());
//echo $client->getLastResponse();