<?php
require_once '../../DB.php';

class ProductsAPI
{
    protected $db;

    public function __construct()
    {
        $this->db = new DB('localhost','root','hatlsmoob','phpcamp');
    }


    public function getAllProducts(){
        $queryString = "SELECT * FROM products";
        $this->db->query($queryString);
        return $this->db->getAllRows();
    }

    public function checkProduct($id){
        $queryString = "SELECT * FROM `products` WHERE `id`='".$id."' LIMIT 1";
        $this->db->query($queryString);

        return $this->db->getRow();
    }

    public function addProduct($name, $price){
        $queryString = "INSERT INTO `products`(`name`,`price`) VALUES('".$name."','".$price."')";
        if($this->db->query($queryString))
            return true;
        else return false;
    }

    public function removeProduct($id){
        $queryString = "DELETE FROM `products` WHERE id=".$id;

        if($this->db->query($queryString))
            return true;
        else return false;
    }
}